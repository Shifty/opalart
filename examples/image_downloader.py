import asyncio
import aiohttp
import opalart


async def download_cycle():
    client = opalart.yandere()
    tags = ['thighhighs', 'kitsune']
    # `limit` dictates how many posts should be fetched. The default is 100.
    # `include_hash` returns a tuple (url, md5) instead of just the url.
    image_urls = await client.getposts(tags, limit=25, include_hash=True)
    for image_url, image_hash in image_urls:
        async with aiohttp.ClientSession() as session:
            async with session.get(image_url) as response:
                image_data = await response.read()
                with open(f'{image_hash}.png', 'wb') as file:
                    file.write(image_data)


loop = asyncio.get_event_loop()
loop.run_until_complete(download_cycle())
loop.close()
